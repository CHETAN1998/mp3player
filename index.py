# -*- coding: utf-8 -*-

import os
#for reading directory files
import tkFileDialog as F
#for selecting directory
from Tkinter import *
#ui
import pygame
#for playing audio file
from mutagen.id3 import ID3
#for exctracting metadata
import tkMessageBox
#dislaying notification

root = Tk()
#Creating instance of Tk class
root.wm_title("MUSIC PLAYER")
#title for the player
root.minsize(500, 500)
#window size

listofsongs = []
#list of song files
realnames = []
#list of name of songs

v = StringVar()
songlabel = Label(root, textvariable = v, width = 80)
#label for displaying current song
index = 0
count = 0

global ctr
ctr = 0
#play and pause flag

def updatelabel():
    global index
    global songname
    v.set(listofsongs[index])
    #updating current song

def pausesong(event):
    #pausing the song
    global ctr
    ctr += 1
    if (ctr % 2 != 0):
        pygame.mixer.music.pause()
    if(ctr % 2 == 0):
        pygame.mixer.music.unpause()

def playsong(event):
    #playing song
    pygame.mixer.music.play()


def nextsong(event):
    #changing song
    global index
    index += 1
    if (index < count):
        pygame.mixer.music.load(listofsongs[index])
        pygame.mixer.music.play()
    else:
        index = 0
        pygame.mixer.music.load(listofsongs[index])
        pygame.mixer.music.play()
    try:
      updatelabel()
    except NameError:
        print("")

def previoussong(event):
    #changing song
    global index
    index -= 1
    pygame.mixer.music.load(listofsongs[index])
    pygame.mixer.music.play()
    try:
        updatelabel()
    except NameError:
        print("")


def stopsong(event):
    #stopping song
    pygame.mixer.music.stop()

def mute(event):
    #set volume to zero
    vol.set(0)

scrollbar = Scrollbar(root)
scrollbar.pack( side = RIGHT, fill = Y )

label = Label(root, text = "Music Player")
label.pack()

listbox = Listbox(root, yscrollcommand = scrollbar.set, selectmode = MULTIPLE, width = 100, height = 20, bg = "grey", fg = "black")
listbox.pack(fill = X)
scrollbar.config( command = listbox.yview )


def directorychooser():
    #changing directory
  global count
  global index
    #count=0

  directory = F.askdirectory()
  if(directory):
    count = 0
    index = 0
    #listbox.delete(0, END)
    del listofsongs[:]
    del realnames[:]

    os.chdir(directory)

    for  files in os.listdir(directory):
        #searching the file for .mp3 files
        try:
         if files.endswith(".mp3") or files.endswith(".mp4"):
              listofsongs.append(files)
              realdir = os.path.realpath(files)
              audio = ID3(realdir)
              realnames.append(audio['TIT2'].text[0])
        except:
            pass

    if listofsongs == [] :
       okay = tkMessageBox.askretrycancel("No songs found", "no songs")
       if(okay == True):
           directorychooser()

    else:
        listbox.delete(0, END)
        realnames.reverse()
        for items in realnames:
            listbox.insert(0, items)
        for i in listofsongs:
            count = count + 1
        pygame.mixer.init()
        pygame.mixer.music.load(listofsongs[0])
        pygame.mixer.music.play()
        try:
            updatelabel()
        except NameError:
            print("")
  else:
    return 1

try:
       directorychooser()
except WindowsError:
         print("thank you")


def call(event):
    if(True):
        try:
            k = directorychooser()
        except WindowsError:
         print("thank you")

realnames.reverse()
songlabel.pack()

def show_value(self):
    i = vol.get()
    pygame.mixer.music.set_volume(i)
var = DoubleVar()
vol = Scale(root, from_ = 100, to = 0, orient = VERTICAL , resolution = 1, command = show_value, variable = var)
vol.place(x = 85, y = 380)
j = var.get()
if j > 0 and j < 30:
    vol.set(10)
elif j >= 30 and j < 50:
    vol.set(10)
elif j >= 50 and j < 70:
    vol.set(90)
elif j >= 70:
    vol.set(90)

framemiddle = Frame(root, width = 250, height = 30)
framemiddle.pack()

framedown = Frame(root, width = 400, height = 300)
framedown.pack()

openbutton = Button(framedown, text = "open")
openbutton.pack(side = LEFT)

mutebutton = Button(framedown, text = u"\U0001F507")
mutebutton.pack(side = LEFT)

previousbutton = Button(framedown, text = "◄◄")
previousbutton.pack(side = LEFT)

playbutton = Button(framedown, text = "►")
playbutton.pack(side = LEFT)

stopbutton = Button(framedown, text = "■")
stopbutton.pack(side = LEFT)

nextbutton = Button(framedown, text = "►►")
nextbutton.pack(side = LEFT)

pausebutton = Button(framedown, text = "►/║║")
pausebutton.pack(side = LEFT)

mutebutton.bind("<Button-1>", mute)
openbutton.bind("<Button-1>", call)
playbutton.bind("<Button-1>", playsong)
nextbutton.bind("<Button-1>", nextsong)
previousbutton.bind("<Button-1>", previoussong)
stopbutton.bind("<Button-1>", stopsong)
pausebutton.bind("<Button-1>", pausesong)

root.mainloop()
